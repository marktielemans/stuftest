package nl.marktielemans.stuftest.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class XMLTestSettings implements IsSerializable {
	protected String xml;
	protected String URL;
	protected String privateKeyPath;
	protected String privateKeyPass;
	protected String publicKeyPath;
	protected String publicKeyPass;
	protected String soapAction;

	public XMLTestSettings() {}

	public XMLTestSettings(String xml, String uRL, String privateKeyPath,
			String privateKeyPass, String publicKeyPath, String publicKeyPass,
			String soapAction) {
		super();
		this.xml = xml;
		URL = uRL;
		this.privateKeyPath = privateKeyPath;
		this.privateKeyPass = privateKeyPass;
		this.publicKeyPath = publicKeyPath;
		this.publicKeyPass = publicKeyPass;
		this.soapAction = soapAction;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getPrivateKeyPath() {
		return privateKeyPath;
	}

	public void setPrivateKeyPath(String privateKeyPath) {
		this.privateKeyPath = privateKeyPath;
	}

	public String getPrivateKeyPass() {
		return privateKeyPass;
	}

	public void setPrivateKeyPass(String privateKeyPass) {
		this.privateKeyPass = privateKeyPass;
	}

	public String getPublicKeyPath() {
		return publicKeyPath;
	}

	public void setPublicKeyPath(String publicKeyPath) {
		this.publicKeyPath = publicKeyPath;
	}

	public String getPublicKeyPass() {
		return publicKeyPass;
	}

	public void setPublicKeyPass(String publicKeyPass) {
		this.publicKeyPass = publicKeyPass;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}
}