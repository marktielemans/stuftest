package nl.marktielemans.stuftest.server.rpc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.net.ssl.HttpsURLConnection;

import nl.marktielemans.stuftest.client.rpc.XMLTestService;
import nl.marktielemans.stuftest.server.ssl.SSLConnectionFactory;
import nl.marktielemans.stuftest.shared.XMLTestSettings;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class XMLTestServiceImpl extends RemoteServiceServlet implements XMLTestService {

	private static final Logger logger = Logger.getLogger(XMLTestServiceImpl.class);
	
	public String testXML(final XMLTestSettings settings) throws IOException {
		final HttpsURLConnection conn = SSLConnectionFactory.getConnection(settings);
		
		InputStream responseStream;
        // Send request
        final OutputStream out = conn.getOutputStream();
        final Writer wout = new OutputStreamWriter(out);
        try {
            wout.write(settings.getXml());
            wout.flush();
        } finally {
            wout.close();
        }

        // Receive response
        try {
            responseStream = conn.getInputStream();
            logger.debug("Response stream: " + responseStream.hashCode());
        } catch (IOException e) {
            logger.error("Error connecting to SOAP service", e);

            // Get error message and log it.
            InputStream resStream = conn.getErrorStream();
            InputStreamReader isr = new InputStreamReader(resStream);
            BufferedReader br = new BufferedReader(isr);
            StringBuffer buf = new StringBuffer();
            String line = null;
            while((line = br.readLine()) != null) {
                buf.append(line);
            }
            logger.error(buf.toString());

            // bubble up the exception
            throw new IOException(e.getMessage());
        }
        
        return IOUtils.toString(responseStream, "UTF-8");
	}
}
