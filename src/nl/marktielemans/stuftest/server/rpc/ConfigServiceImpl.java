package nl.marktielemans.stuftest.server.rpc;

import java.io.File;

import nl.marktielemans.stuftest.client.rpc.ConfigService;
import nl.marktielemans.stuftest.server.ApplicationContext;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class ConfigServiceImpl extends RemoteServiceServlet implements ConfigService {

	public String[] getPrivateKeys() {
		final File[] stores = ApplicationContext.get().getSSLKeystores().getPrivateKeyStores();
		final String[] storeNames = new String[stores.length];
		
		for (int i = 0; i < stores.length; i++) {
			storeNames[i] = stores[i].getName();
		}

		return storeNames;
	}

	public String[] getPublicKeys() {
		final File[] stores = ApplicationContext.get().getSSLKeystores().getPublicKeyStores();
		final String[] storeNames = new String[stores.length];
		
		for (int i = 0; i < stores.length; i++) {
			storeNames[i] = stores[i].getName();
		}

		return storeNames;
	}
	
	public String[] getEndpointURLs() {
		final String[] urls = new String[3];
		urls[0] = "https://luton0.locgov.nl:8443/CGS/StUF/0301/BG/0310/services/BeantwoordVraag/";
		urls[1] = "https://marsham0.locgov.nl:8443/CGS/StUF/0301/BG/0310/services/BeantwoordVraag/";
		urls[2] = "https://weepingwater:54503/StUF3.DDS.TEST/ServiceBeantwoordVraag.svc";
		return urls;
	}

	public String[] getSOAPActions() {
		final String[] actions = new String[6];
		actions[0] = "http://www.egem.nl/StUF/sector/bg/0310/npsLv01Integraal";
	    actions[1] = "http://www.egem.nl/StUF/sector/bg/0310/npsLv01";
	    actions[2] = "http://www.egem.nl/StUF/sector/bg/0310/nnpLv01Integraal";
	    actions[3] = "http://www.egem.nl/StUF/sector/bg/0310/nnpLv01";
	    actions[4] = "http://www.egem.nl/StUF/sector/bg/0310/vesLv01Integraal";
	    actions[5] = "http://www.egem.nl/StUF/sector/bg/0310/vesLv01";
	    return actions;
	}
}
