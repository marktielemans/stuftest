package nl.marktielemans.stuftest.server;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

public class StUFTestUtils {
	
	private static final Logger logger = Logger.getLogger(StUFTestUtils.class);
	
	public static boolean createFolderIfNotExists(final File folder) throws IOException {
		final boolean alreadyExists = folder.exists() && folder.isDirectory();
		
		if (!alreadyExists) {
			logger.debug("Folder " + folder.getAbsolutePath() + " doesn't exist, creating..");
			final boolean created = folder.mkdir();
			
			if (!created) {
				throw new IOException("Failed to create new folder at " + folder.getAbsolutePath());
			}
		}
		
		return alreadyExists;
	}
}
