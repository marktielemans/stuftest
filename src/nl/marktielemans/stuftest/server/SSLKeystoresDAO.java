package nl.marktielemans.stuftest.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;

import org.apache.log4j.Logger;

public class SSLKeystoresDAO {
	
	private static final String KEYSTORES_FOLDER_PATH = "/sslKeystores";
	private static final String PRIVATE_KEYSTORES_FOLDER_PATH = KEYSTORES_FOLDER_PATH + "/private";
	private static final String PUBLIC_KEYSTORES_FOLDER_PATH = KEYSTORES_FOLDER_PATH + "/public";
	private static final Logger logger = Logger.getLogger(SSLKeystoresDAO.class);
	
	protected File keystoresFolder;
	protected File privateStoresFolder;
	protected File publicStoresFolder;
	
	public SSLKeystoresDAO() {
		init();
	}
	
	protected void init() {
		this.keystoresFolder = new File(KEYSTORES_FOLDER_PATH);
		try {
			final boolean created = !StUFTestUtils.createFolderIfNotExists(keystoresFolder);
			if (created) {
				logger.info("Created keystores root folder at " + this.keystoresFolder.getAbsolutePath());
			}
		} catch (final IOException ioe) {
			logger.error("Failed to create keystores root folder");
			throw new RuntimeException("Failed to create keystores root folder", ioe);
		}
		
		// Create private stores folder
		this.privateStoresFolder = new File(PRIVATE_KEYSTORES_FOLDER_PATH);
		try {
			final boolean created = !StUFTestUtils.createFolderIfNotExists(privateStoresFolder);
			if (created) {
				logger.info("Created private keystores folder at " + this.privateStoresFolder.getAbsolutePath());
			}
		} catch (final IOException ioe) {
			logger.error("Failed to create private keystores folder");
			throw new RuntimeException("Failed to create private keystores folder", ioe);
		}
		
		// Create public stores folder
		this.publicStoresFolder = new File(PUBLIC_KEYSTORES_FOLDER_PATH);
		try {
			final boolean created = !StUFTestUtils.createFolderIfNotExists(publicStoresFolder);
			if (created) {
				logger.info("Created public keystores folder at " + this.publicStoresFolder.getAbsolutePath());
			}
		} catch (final IOException ioe) {
			logger.error("Failed to create public keystores folder");
			throw new RuntimeException("Failed to create public keystores folder", ioe);
		}
		
		logger.info("SSL Keystores service initialized");
	}
	
	protected String deriveKeystoreType(final String path, final String defaultValue) {
		if (path.endsWith("jks")) {
			return "jks";
		} 

		if (path.endsWith("truststore")) {
			return "jks";
		}

		if (path.endsWith("p12")) {
			return "pkcs12";
		}

		if (path.endsWith("pfx")) {
			return "pkcs12";
		}

		return defaultValue;
	}

	public KeyStore getKeyStore(final String path, final String pwd, final String type) throws IOException, KeyStoreException {
		if (type == null) {
			throw new NullPointerException("KeyStore type not provided");
		}

		logger.debug("Fetching SSL keystore at: " + path + " as type: " + type);

		final char[] pass = pwd.toCharArray();

		// Next, set up the KeyStore to use. We need to load the file into
		// a KeyStore instance.
		final KeyStore keyStore = KeyStore.getInstance(type);
		final FileInputStream fis = new FileInputStream(path);
		try {
			keyStore.load(fis, pass);
		} catch (final GeneralSecurityException e) {
			throw new RuntimeException("Failed to load keystore", e);
		} finally {
			fis.close();
		}

		return keyStore; 
	}
	
	public KeyStore getPrivateKeyStore(final String path, final String pwd) throws KeyStoreException, IOException {
		return getKeyStore(path, pwd, deriveKeystoreType(path, "pkcs12"));
	}
	
	public KeyStore getPublicKeyTrustStore(final String path, final String pwd) throws KeyStoreException, IOException {
		return getKeyStore(path, pwd, deriveKeystoreType(path, "jks"));
	}
	
	public File[] getPublicKeyStores() {
		return this.publicStoresFolder.listFiles();
	}
	
	public File[] getPrivateKeyStores() {
		return this.privateStoresFolder.listFiles();
	}
}
