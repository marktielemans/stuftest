package nl.marktielemans.stuftest.server;

public class ApplicationContext {
	private static ApplicationContext instance;
	
	private SSLKeystoresDAO sslKeystores = new SSLKeystoresDAO();
	
	protected ApplicationContext() {}
	
	public SSLKeystoresDAO getSSLKeystores() {
		return this.sslKeystores;
	}
	
	public static ApplicationContext get() {
		if (instance == null) {
			instance = new ApplicationContext();
		}
		return instance;
	}
}
