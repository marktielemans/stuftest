package nl.marktielemans.stuftest.server.ssl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.ssl.SSLSocketFactory;

/**
 * Wraps a {@link SSLSocketFactory} and allows child classes to reconfigure all sockets created by the delegate 
 * factory in their own way.
 * @author Mark Tielemans
 *
 */
public abstract class ConfigurableSocketFactory extends SSLSocketFactory {
	
	private SSLSocketFactory delegateSocketFactory;

	public ConfigurableSocketFactory(final SSLSocketFactory sslSocketFactory) {
		this.delegateSocketFactory = sslSocketFactory;
	}

	@Override
	public Socket createSocket(final Socket s, final String host, final int port,
			final boolean autoClose) throws IOException {
		return configureSocket(this.delegateSocketFactory.createSocket(s, host, port, autoClose));
	}

	@Override
	public String[] getDefaultCipherSuites() {
		return this.delegateSocketFactory.getDefaultCipherSuites();
	}

	@Override
	public String[] getSupportedCipherSuites() {
		return this.getSupportedCipherSuites();
	}

	@Override
	public Socket createSocket(final String host, final int port) throws IOException, UnknownHostException {
		return configureSocket(this.delegateSocketFactory.createSocket(host, port));

	}

	@Override
	public Socket createSocket(final InetAddress host, final int port) throws IOException {
		return configureSocket(this.delegateSocketFactory.createSocket(host, port));
	}

	@Override
	public Socket createSocket(final String host, final int port, final InetAddress localHost,
			final int localPort) throws IOException, UnknownHostException {
		return configureSocket(this.delegateSocketFactory.createSocket(host, port, localHost, localPort));
	}

	@Override
	public Socket createSocket(final InetAddress address, final int port,
			final InetAddress localAddress, final int localPort) throws IOException {
		return configureSocket(this.delegateSocketFactory.createSocket(address, port, localAddress, localPort));
	}

	/**
	 * Each socket created by this factory is passed through this method to allow for last-minute configuration. 
	 * If an implementation does not wish to reconfigure the socket, it should simply pass the instance through 
	 * as-is.
	 * @param socket the socket
	 * @return the reconfigured socket
	 */
	protected abstract Socket configureSocket(final Socket socket);
}