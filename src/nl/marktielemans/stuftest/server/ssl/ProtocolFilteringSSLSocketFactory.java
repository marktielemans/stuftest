package nl.marktielemans.stuftest.server.ssl;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.log4j.Logger;

/**
 * SSL socket factory that filters the SSL protocols enabled on the sockets created by the delegate factory 
 * against a list of allowed protocols {@link ProtocolFilteringSSLSocketFactory#enabledProtocols}. By default 
 * only <code>SSLv3</code> and <code>TLSv1</code> are allowed. This behaviour is changed by setting a different 
 * filter.
 * 
 * @author Mark Tielemans
 */
public class ProtocolFilteringSSLSocketFactory extends ConfigurableSocketFactory {

	private static final Logger logger = Logger.getLogger(ProtocolFilteringSSLSocketFactory.class);
	
	private List<String> enabledProtocols;
	
	/**
	 * Create a new socket factory with the default settings.
	 * @param sslSocketFactory
	 */
	public ProtocolFilteringSSLSocketFactory(final SSLSocketFactory sslSocketFactory) {
		this(sslSocketFactory, "TLSv1", "SSLv3");
	}
	
	/**
	 * Create a new socket factory specifying the protocols that should be allowed.
	 * @param sslSocketFactory
	 * @param enabledProtocols
	 */
	public ProtocolFilteringSSLSocketFactory(final SSLSocketFactory sslSocketFactory, final String... enabledProtocols) {
		super(sslSocketFactory);
		this.enabledProtocols = Arrays.asList(enabledProtocols);
	}

	@Override
	protected Socket configureSocket(final Socket socket) {
		// Filter out all disabled protocols
		final SSLSocket sslSocket = ((SSLSocket) socket) ;
		final String[] protocols = sslSocket.getEnabledProtocols();
		final List<String> resumedProtocols = new ArrayList<String>(protocols.length);

		for (final String protocol : protocols) {
			if (this.enabledProtocols.contains(protocol)) {
				logger.debug("Enabled " + protocol + " protocol");
				resumedProtocols.add(protocol);
			} else {
				logger.debug("Disabled " + protocol + " protocol");
			}
		}
		
		sslSocket.setEnabledProtocols(resumedProtocols.toArray(new String[resumedProtocols.size()]));
		return sslSocket;
	}

	/**
	 * Get the enabledProtocols
	 * @return the enabledProtocols
	 */
	public List<String> getEnabledProtocols() {
		return enabledProtocols;
	}

	/**
	 * Set the enabledProtocols
	 * @param enabledProtocols the new enabledProtocols
	 */
	public void setEnabledProtocols(List<String> enabledProtocols) {
		this.enabledProtocols = enabledProtocols;
	}
}
