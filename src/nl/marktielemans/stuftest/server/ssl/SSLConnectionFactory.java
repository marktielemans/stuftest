package nl.marktielemans.stuftest.server.ssl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import nl.marktielemans.stuftest.server.ApplicationContext;
import nl.marktielemans.stuftest.shared.XMLTestSettings;

import org.apache.log4j.Logger;

public class SSLConnectionFactory {

	private static final Logger logger = Logger.getLogger(SSLConnectionFactory.class);

	private static TrustManager[] getTrustManagers(final XMLTestSettings settings) throws IOException {
		try {
			logger.debug("Using truststore " + settings.getPublicKeyPath());

			// First, get the default TrustManagerFactory.
			final String algorithm = TrustManagerFactory.getDefaultAlgorithm();
			final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(algorithm);

			// Next, set up the TrustStore to use. We need to load the file into
			// a KeyStore instance.
			final KeyStore ks = ApplicationContext.get().getSSLKeystores()
					.getPublicKeyTrustStore(settings.getPublicKeyPath(), settings.getPublicKeyPass());

			// Now we initialize the TrustManagerFactory with this KeyStore
			trustManagerFactory.init(ks);

			// And now get the TrustManagers
			final TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
			return trustManagers;
		} catch (GeneralSecurityException e) {
			throw new IOException("Unable to get trustmanagers", e);
		}
	}

	private static KeyManager[] getKeyManagers(final XMLTestSettings settings) throws IOException {
		try {
			logger.debug("Using private keystore: " + settings.getPrivateKeyPath());

			// First, get the default KeyManagerFactory.
			final String algorithm = KeyManagerFactory.getDefaultAlgorithm();
			final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(algorithm);

			// Next, set up the KeyStore to use. We need to load the file into
			// a KeyStore instance.
			final KeyStore ks = ApplicationContext.get().getSSLKeystores()
					.getPrivateKeyStore(settings.getPrivateKeyPath(), settings.getPrivateKeyPass());

			// Now we initialize the TrustManagerFactory with this KeyStore
			keyManagerFactory.init(ks, settings.getPrivateKeyPass().toCharArray());

			// And now get the TrustManagers
			final KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
			return keyManagers;
		} catch (GeneralSecurityException e) {
			throw new IOException("Unable to get keymanagers.", e);
		}
	}

	/**
	 * Retrieve an SSLSocketFactory which either provides or expects a certificate, or both.
	 * @param provideCertificate true if the SSL certificate for OPEN.post should be included
	 * @param expectCertificate true if the truststore for OPEN.post should be included
	 * @return the SSLSocketFactory of an initialized SSLContext with the given parameters
	 * @throws IOException
	 * @since 1
	 * @see #getKeyManagers()
	 * @see #getTrustManagers()
	 */
	private static SSLSocketFactory getSSLSocketFactory(final XMLTestSettings settings) throws IOException {
		final boolean provideKey = settings.getPrivateKeyPath() != null;
		final boolean expectKey = settings.getPublicKeyPath() != null;

		// Call getTrustManagers to get suitable trust managers
		TrustManager[] trustManagers = null;
		if (expectKey) {
			trustManagers = getTrustManagers(settings);
		}

		// Call getKeyManagers to get suitable key managers
		KeyManager[] keyManagers = null;
		if (provideKey) {
			keyManagers = getKeyManagers(settings);
		}

		// Next construct and initialize a SSLContext with the KeyStore and
		// the TrustStore. We use the default SecureRandom.
		SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("TLSv1");
			logger.debug("SSL protocol: " + sslContext.getProtocol());

			// Init context
			sslContext.init(keyManagers, trustManagers, null);
		} catch (NoSuchAlgorithmException e) {
			throw new IOException(e);
		} catch (KeyManagementException e) {
			throw new IOException(e);
		}

		// Finally, we get a SocketFactory, and pass it to SimpleSSLClient.
		final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

		// This SSLFactory wrapper ensures only TLSv1 is ever used (no SSL hello)
		return new ProtocolFilteringSSLSocketFactory(sslSocketFactory, "TLSv1");
	}

	/**
	 *
	 * @param url connection url
	 * @param security security level, defaulting to {@link ESSLSecurity#TWOWAY} if null
	 * @return
	 * @throws IOException
	 * @see FileNotFoundException
	 */
	public static HttpsURLConnection getConnection(final XMLTestSettings settings) throws IOException {
		final SSLSocketFactory sslsocketfactory = getSSLSocketFactory(settings);
		final URL httpsUrl = new URL(settings.getURL());

		// Create connection with optional proxy settings
		final HttpsURLConnection conn = (HttpsURLConnection) httpsUrl.openConnection();
		//			Proxy proxy = null;

		//			if (!StringUtils.isBlank(OPSettings.getHTTPS_PROXY_HOST())) {
		//				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
		//						OPSettings.getHTTPS_PROXY_HOST(), 
		//						OPSettings.getHTTPS_PROXY_PORT()));
		//			}
		//
		//			if (proxy != null) {
		//				conn = (HttpsURLConnection) httpsUrl.openConnection(proxy);
		//			} else {
		//				conn = (HttpsURLConnection) httpsUrl.openConnection();
		//			}

		// Add SSL socket factory and return
		conn.setSSLSocketFactory(sslsocketfactory);
		return conn;
	}
}
