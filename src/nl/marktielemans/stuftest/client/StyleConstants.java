package nl.marktielemans.stuftest.client;

import com.google.gwt.user.client.Window;
import com.sencha.gxt.core.client.util.Margins;

public class StyleConstants {
	public static final StyleConstants INSTANCE = new StyleConstants();
	
	public StyleConstants() {}
	
	public Margins stdFormMargins() {
		final String userAgentString = Window.Navigator.getUserAgent();
		final int topMargin = 7;
		final int rightMargin = 0;
		final int bottomMargin = 0;
		final int leftMargin = 0;

		if (userAgentString.contains("Chrome") && userAgentString.contains("chrome")) {
			return new Margins(topMargin + 5, rightMargin, bottomMargin, leftMargin);
		}
		return new Margins(topMargin, rightMargin, bottomMargin, leftMargin);
	}
	
	public Margins smallFormMargins() {
		final Margins margins = this.stdFormMargins();
		margins.setTop(margins.getTop() - 3);
		System.out.println("small margin");
		return margins;
	}
}
