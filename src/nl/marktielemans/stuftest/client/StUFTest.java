package nl.marktielemans.stuftest.client;

import nl.marktielemans.stuftest.client.rpc.ConfigService;
import nl.marktielemans.stuftest.client.rpc.ConfigServiceAsync;
import nl.marktielemans.stuftest.client.rpc.XMLTestService;
import nl.marktielemans.stuftest.client.rpc.XMLTestServiceAsync;
import nl.marktielemans.stuftest.client.ui.TabbedDashboard;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootLayoutPanel;

public class StUFTest implements EntryPoint {
	
	public static final ConfigServiceAsync ConfigService = GWT.create(ConfigService.class);
	public static final XMLTestServiceAsync XMLTestService = GWT.create(XMLTestService.class);
	
	@Override
	public void onModuleLoad() {
		RootLayoutPanel.get().clear();
		RootLayoutPanel.get().add(new TabbedDashboard());
	}
}
