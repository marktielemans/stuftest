package nl.marktielemans.stuftest.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.form.TextArea;

public class XMLEditor extends Composite {

	private static XMLEditorUiBinder uiBinder = GWT
			.create(XMLEditorUiBinder.class);

	interface XMLEditorUiBinder extends UiBinder<Widget, XMLEditor> {
	}

	protected @UiField TextArea editor;
	protected @UiField Element html;
	protected @UiField Element htmlContainer;
	
	public XMLEditor() {
		initWidget(uiBinder.createAndBindUi(this));
		
		editor.setWidth(Window.getClientWidth() / 2);
		editor.setHeight(Window.getClientHeight() - 150);
		
		htmlContainer.getStyle().setWidth(Window.getClientWidth() / 2, Unit.PX);
		htmlContainer.getStyle().setHeight(Window.getClientHeight() - 150, Unit.PX);
		
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent arg0) {
				editor.setWidth(Window.getClientWidth() / 2);
				editor.setHeight(Window.getClientHeight() - 150);
				htmlContainer.getStyle().setWidth(Window.getClientWidth() / 2, Unit.PX);
				htmlContainer.getStyle().setHeight(Window.getClientHeight() - 150, Unit.PX);
			}
		});
	}

	@Override
	protected void onAttach() {
		super.onAttach();
	}

	@UiHandler("editor")
	protected void onEditorKeyDown(final KeyDownEvent event) {
		this.html.setInnerText(editor.getCurrentValue());
		this.highlight(html);
	}
	
	@UiHandler("editor")
	protected void onEditorKeyUp(final KeyUpEvent event) {
		this.html.setInnerText(editor.getCurrentValue());
		this.highlight(html);
	}
	
	@UiHandler("editor")
	protected void onEditorKeyPress(final KeyPressEvent event) {
		this.html.setInnerText(editor.getCurrentValue());
		this.highlight(html);
	}
	
	@UiHandler("editor")
	protected void onEditorValueChanged(final ValueChangeEvent<String> event) {
		this.html.setInnerText(editor.getCurrentValue());
		this.highlight(html);
	}
	
	public native void highlight(final Element el) /*-{
	  $wnd.hljs.highlightBlock(el);
	}-*/;
	
	public String getXML() {
		return this.editor.getCurrentValue();
	}
}
