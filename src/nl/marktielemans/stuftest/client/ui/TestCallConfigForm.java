package nl.marktielemans.stuftest.client.ui;

import java.util.Date;

import nl.marktielemans.stuftest.client.StUFTest;
import nl.marktielemans.stuftest.shared.XMLTestSettings;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.IntegerPropertyEditor;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

public class TestCallConfigForm extends Composite {

	private static TestCallConfigFormUiBinder uiBinder = GWT
			.create(TestCallConfigFormUiBinder.class);

	interface TestCallConfigFormUiBinder extends UiBinder<Widget, TestCallConfigForm> {
	}
	
	protected Date runningCallStart;

	protected @UiField TextButton btnCancel;
	protected @UiField TextButton btnExecute;
	
	protected @UiField ComboBox<String> cbSoapAction;
	protected @UiField ComboBox<String> cbEndpointURL;
	protected @UiField(provided = true) ListStore<String> endpoints = new ListStore<String>(new ModelKeyProvider<String>() {
		public String getKey(String item) {
			return item;
		}
	});
	protected @UiField(provided = true) ListStore<String> actions = new ListStore<String>(new ModelKeyProvider<String>() {
		public String getKey(String item) {
			return item;
		}
	});
	protected @UiField(provided = true) LabelProvider<String> labels = new LabelProvider<String>() {
		public String getLabel(String item) {
			return item;
		}
	};
	
	protected @UiField ListBox lbSSLPrivateKey;
	protected @UiField ListBox lbSSLPublicKey;
	protected @UiField PasswordField pwPublicKey;
	protected @UiField PasswordField pwPrivateKey;
	
	protected @UiField TextField tfProxyHost;
	protected @UiField(provided = true) NumberField<Integer> tfProxyPort;

	public TestCallConfigForm() {
		this.initEndpointURLOptions();
		this.initSOAPActionOptions();
		this.preUIInit();
		this.init();
	}

	protected void preUIInit() {
		this.tfProxyPort = new NumberField<Integer>(new IntegerPropertyEditor());
	}

	protected void init() {
		initWidget(uiBinder.createAndBindUi(this));

		// Load private keys
		StUFTest.ConfigService.getPrivateKeys(new AsyncCallback<String[]>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
			}

			public void onSuccess(String[] results) {
				for (final String result : results) {
					lbSSLPrivateKey.addItem(result);
				}

				lbSSLPrivateKey.setEnabled(true);
			}
		});

		// Load public keys
		StUFTest.ConfigService.getPublicKeys(new AsyncCallback<String[]>() {
			public void onFailure(final Throwable caught) {
				Window.alert(caught.getMessage());
			}

			public void onSuccess(final String[] results) {
				for (final String result : results) {
					lbSSLPublicKey.addItem(result);
				}

				lbSSLPublicKey.setEnabled(true);
			}
		});
	}
	
	public TextButton getExecuteButton() {
		return this.btnExecute;
	}
	
	public TextButton getCancelButton() {
		return this.btnCancel;
	}

	protected void initEndpointURLOptions() {
		// Load endpoint URLs
		StUFTest.ConfigService.getEndpointURLs(new AsyncCallback<String[]>() {
			public void onFailure(final Throwable caught) {
				Window.alert(caught.getMessage());
			}

			public void onSuccess(final String[] results) {
				for (final String result : results) {
					endpoints.add(result);
				}
			}
		});
	}
	
	protected void initSOAPActionOptions() {
		// Load endpoint URLs
		StUFTest.ConfigService.getSOAPActions(new AsyncCallback<String[]>() {
			public void onFailure(final Throwable caught) {
				Window.alert(caught.getMessage());
			}

			public void onSuccess(final String[] results) {
				for (final String result : results) {
					actions.add(result);
				}
			}
		});
	}

	@UiHandler("cbUseProxy")
	protected void onUseProxyChange(final ValueChangeEvent<Boolean> event) {
		tfProxyHost.setEnabled(event.getValue());
		tfProxyPort.setEnabled(event.getValue());
	}
	
	public XMLTestSettings read() {
		final XMLTestSettings settings = new XMLTestSettings();
		settings.setPrivateKeyPath(this.lbSSLPrivateKey.getSelectedValue());
		settings.setPrivateKeyPass(this.pwPrivateKey.getCurrentValue());
		settings.setPublicKeyPath(this.lbSSLPublicKey.getSelectedValue());
		settings.setPublicKeyPass(this.pwPublicKey.getCurrentValue());
		settings.setSoapAction(this.cbSoapAction.getCurrentValue());
		settings.setURL(this.cbEndpointURL.getCurrentValue());
		return settings;
	}
	
}
