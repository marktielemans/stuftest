package nl.marktielemans.stuftest.client.ui;

import nl.marktielemans.stuftest.client.StUFTest;
import nl.marktielemans.stuftest.shared.XMLTestSettings;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.TextArea;

public class TabbedDashboard extends Composite {

	private static TabbedDashboardUiBinder uiBinder = GWT
			.create(TabbedDashboardUiBinder.class);

	interface TabbedDashboardUiBinder extends UiBinder<Widget, TabbedDashboard> {
	}
	
	protected @UiField XMLEditor editor;
	protected @UiField TestCallConfigForm configForm;

	public TabbedDashboard() {
		initWidget(uiBinder.createAndBindUi(this));
		
		configForm.getExecuteButton().addSelectHandler(new SelectHandler() {
			public void onSelect(final SelectEvent event) {
				testXML();
			}
		});
	}
	
	protected void testXML() {
		configForm.getExecuteButton().setEnabled(false);
		final XMLTestSettings settings = configForm.read();
		settings.setXml(editor.getXML());
		StUFTest.XMLTestService.testXML(settings, new AsyncCallback<String>() {
			public void onFailure(final Throwable caught) {
				com.google.gwt.user.client.Window.alert(caught.getMessage());
				configForm.getExecuteButton().setEnabled(true);
			}

			public void onSuccess(final String result) {
				Window window = new Window();
				window.setSize("1800px", "1000px");
				window.setClosable(true);
				
				final TextArea ta = new TextArea();
				ta.setSize("1800px", "1000px");
				ta.setValue(result);
				window.setWidget(ta);
				window.show();
				
				configForm.getExecuteButton().setEnabled(true);
			}
		});
	}
}
