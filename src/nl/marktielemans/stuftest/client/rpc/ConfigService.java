package nl.marktielemans.stuftest.client.rpc;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("rpc/config")
public interface ConfigService extends RemoteService {
	public String[] getPrivateKeys();
	public String[] getPublicKeys();
	public String[] getEndpointURLs();
	public String[] getSOAPActions();
}
