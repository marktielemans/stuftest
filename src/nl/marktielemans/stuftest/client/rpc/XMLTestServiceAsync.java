package nl.marktielemans.stuftest.client.rpc;

import nl.marktielemans.stuftest.shared.XMLTestSettings;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface XMLTestServiceAsync {
	public void testXML(final XMLTestSettings settings, final AsyncCallback<String> callback);
}
