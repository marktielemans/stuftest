package nl.marktielemans.stuftest.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ConfigServiceAsync {
	public void getPrivateKeys(final AsyncCallback<String[]> callback);
	public void getPublicKeys(final AsyncCallback<String[]> callback);
	public void getEndpointURLs(final AsyncCallback<String[]> callback);
	public void getSOAPActions(final AsyncCallback<String[]> callback);
}
