package nl.marktielemans.stuftest.client.rpc;

import java.io.IOException;

import nl.marktielemans.stuftest.shared.XMLTestSettings;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("rpc/xmltest")
public interface XMLTestService extends RemoteService {
	public String testXML(final XMLTestSettings settings) throws IOException;
}